menu_utama:
   call reset
   call menu
   call minta_input
   cmp ch,1
   je function_tambah_data
   cmp ch,2
   je function_cetak_semua_data
   cmp ch,3
   je function_cetak_data_tertentu
   cmp ch,4
jne menu_utama
int 20h 

function_tambah_data:
   call tambah
   mov ah,9
   mov dx,offset enter
   int 21h
jmp menu_utama
enter db 10,13,'$'

function_cetak_semua_data:
   call cetak_all
   mov ah,9
   mov dx,offset enter
   int 21h
jmp menu_utama

tampilan_menu db 'menu:',10,13,'1.Tambah Data',10,13,'2.Cetak Semua Data',10,13,'3.Cetak Data Tertentu',10,13,'4.exit',10,13,'>>>','$'

text_tambah_nama db 'nama :','$'
text_tambah_poin db 10,13,'poin :','$'
 
tambah proc
    jc errmsg
    
    mov ah,9
    mov dx,offset text_tambah_nama
    int 21h
     
    mov ah,0ah
    mov dx,offset inputan_nama
    int 21h  
           
    mov ah,9
    mov dx,offset text_tambah_poin
    int 21h
    
    mov ah,0ah
    mov dx,offset inputan_poin
    int 21h
    
    call check_ada
    
    mov ah,3dh
    mov dx,offset filename
    mov al,2h
    int 21h
    mov filehandle,ax
    
    mov ah,42h
    mov al,2h
    mov bx,filehandle
    mov dx,0
    mov cx,0
    int 21h
     
    mov ah,40h
    mov bx,filehandle
    mov dx,offset inputan_nama+2
    mov ch,0
    mov cl,inputan_nama+1
    int 21h
    
    mov ah,40h
    mov bx,filehandle
    mov dx,offset separator_data
    mov ch,0
    mov cl,1
    int 21h
    
    mov ah,40h
    mov bx,filehandle
    mov dx,offset inputan_poin+2
    mov ch,0
    mov cl,inputan_poin+1
    int 21h 
    
    mov ah,40h
    mov bx,filehandle
    mov dx,offset separator
    mov ch,0
    mov cl,1
    int 21h
    
    jmp exit
    
    errmsg: 
    mov ah,9h
    mov dx,offset errormessage
    int 21h
    
    exit:
ret

isifile db 100 dup('$')

check_ada proc
    mov ah,3dh
    mov dx,offset filename
    mov al,2h
    int 21h
    mov filehandle,ax
    
    mov ah, 3fh
    mov bx, filehandle
    mov dx, offset isifile
    mov cx, 100
    int 21h      
    
    mov strlen, al ; selamatkan panjang string 
    
    mov bx, offset isifile
    mov si, 0      ; int 1 = 0
    mov ch, 0
    mov di, 0
    mov cl, strlen  
      
    mov array_posisi[di],0
    mov di,1
      
    isi_array_offset_cret:
      mov ah,isifile[si]
      cmp isifile[si],'|'
      jne exit3
      
      add si,1
      mov array_posisi[di],si
      sub si,1
      inc di
      inc jumlah_data
      
      exit3:
      inc si
      cmp cl,0 
      je exit_isi_array
    loop isi_array_offset_cret
    
    exit_isi_array:
    mov cl,jumlah_data
    mov si,0
        
    cek_data_dalam_array_cret:
      push si
      push cx
      mov dx, offset isifile
      add dx, array_posisi[si]
      mov dh,01h
      mov di,dx
      mov si,offset inputan_nama[2]
      mov cl,inputan_nama[1]
      add dl,cl
      add dl,1
      
      rep cmpsb
      
      je ada_data:
      pop cx
      pop si
      inc si
      cmp cl,0
      je exit_cek_data_dalam_array    
    loop cek_data_dalam_array_cret
    exit_cek_data_dalam_array:
ret
    ada_data:
    
    mov cl,inputan_nama[1]
    pop bx
    pop bx
    mov si,array_posisi[bx]
    push bx
    add si,cx
    mov bx,si
    mov bh,0
    mov si,bx
    mov bx,0
    inc si
    mov di,0
    mov jumlah_digit,0
    ambil_poin_user_cret:
      mov ah,isifile[si]
      cmp isifile[si],'|'
    je exit_ambil_poin
      mov bl,isifile[si]
      mov array_poin[di],bl
      cmp isifile[si],'|'
      inc si
      inc di
      inc jumlah_digit
    jne ambil_poin_user_cret
    
    exit_ambil_poin:
    
    mov cl,jumlah_digit
    dec cl
    mov di,0
    ;ingat mungkin perlu offset isi dx untuk nanti save file
    mov dx,0
    mengubah_poin_awal_user:
        push cx
        mov al,array_poin[di]
        sub al,30h
        mov bl,10d
        hitung_pangkat:
            mul bl
        loop hitung_pangkat
        add dx,ax
        pop cx
        inc di    
    loop mengubah_poin_awal_user
    sub array_poin[di],30h
    add dl,array_poin[di]
    
    mov cl,inputan_poin[1]
    dec cl 
    mov di,2
    menambah_poin_baru:
        push cx
        sub inputan_poin[di],30h
        mov al,inputan_poin[di],30h
        hitung_pangkat2:
            mul bl
        loop hitung_pangkat2
        add dx,ax
        pop cx
        inc di    
    loop menambah_poin_baru
    sub inputan_poin[di],30h
    add dl,inputan_poin[di]
    mov di,0
    
    mov ax,dx 
    mov cl,100d
    div cl
    add al,30h
    mov array_poin_akhir[di],al
    inc di
    mov al,ah
    mov ah,0
    mov cl,10d
    div cl
    add ah,30h
    add al,30h
    mov array_poin_akhir[di],al
    inc di
    mov array_poin_akhir[di],ah
    inc di
    mov array_poin_akhir[di],'$'
    
    mov ah,3eh
    mov bx,filehandle
    int 21h
    
    mov ah,41h
    mov cx,0
    mov dx,offset filename
    int 21h

    mov ah,3ch
    mov dx,offset filename
    mov cx,0
    int 21h

    mov ah,3dh
    mov dx,offset filename
    mov al,2h
    int 21h
    mov filehandle,ax
    
    mov ah,40h
    mov bx,filehandle
    mov dx,offset isifile
    pop si
    mov cx,array_posisi[si]
    mov ch,0
    add cl,inputan_nama[1]
    inc cx
    int 21h
    
    add dx,cx
    push dx
    
    mov ah,40h
    mov bx,filehandle
    mov dx,offset array_poin_akhir
    mov cx,3
    int 21h
    
    pop dx
    add dx,3
    mov ah,40h
    mov bx,filehandle
    mov cx,array_posisi[si]
    mov ch,0
    add cl,inputan_nama[1]
    add cl,3
    mov al,strlen
    sub al,cl
    mov cl,al
    dec cl
    int 21h
jmp menu_utama
filehandle dw 0                   
errormessage db 'ERROR','Mahasiswa tidak ditemukan','$'
filename db 'textfile.txt',0
inputan_nama db 100,1,30 dup('$')
inputan_poin db 100,1,30 dup('$') 
array_posisi dw 20 dup(0) 
separator db '|'
separator_data db '-'
jumlah_data db 0h
jumlah_digit db 0h
array_poin db 5 dup(0)
array_poin_akhir db 5 dup(0) 

cetak_all proc
    mov ah,3dh
    mov dx,offset filename_2
    mov al,2h
    int 21h
    mov filehandle,ax
       
    jc errmsg_2
    
    mov ah, 3fh
    mov bx, filehandle
    mov dx, offset isifile
    mov cx, 100
    int 21h      
    
    mov strlen, al ; selamatkan panjang string 
    
    mov bx, offset isifile
    mov si, 0      ; int 1 = 0
    mov ch, 0
    mov cl, strlen
    mov posisi[di],0
    inc di
      
    replace_cret:
      cmp isifile[si], '|'
      jne skip
        ;mov [bx + si], '$'  cara 1 
        mov isifile[si],10;cara 2
        add si, 1
        mov posisi[di], si
        sub si, 1
        inc di
      skip:
      inc si;
    loop replace_cret     
    
    mov ah, 9           
    mov dx, offset isifile
    int 21h
    
    jmp exit_2:
    errmsg_2: 
    mov ah,9h
    mov dx,offset errormessage_2
    int 21h
    
    exit_2:    
ret
posisi dw 20 dup(0)      
strlen db ?                  
errormessage_2 db 'ERROR','Gagal buka file','$'
filename_2 db 'textfile.txt',0
inputan db 30,1,30 dup('$')   
string db 100,1,100 dup('p')
isifile2 db 100 dup('$') 

function_cetak_data_tertentu proc
    mov ah,9
    mov dx,offset text_tambah_nama
    int 21h
     
    mov ah,0ah
    mov dx,offset inputan_nama
    int 21h
    
    mov ah,3dh
    mov dx,offset filename
    mov al,2h
    int 21h
    mov filehandle,ax
    
    mov ah, 3fh
    mov bx, filehandle
    mov dx, offset isifile2
    mov cx, 100
    int 21h      
    
    mov strlen, al ; selamatkan panjang string 
    
    mov bx, offset isifile2
    mov si, 0      ; int 1 = 0
    mov ch, 0
    mov di, 0
    mov cl, strlen  
      
    mov array_posisi[di],0
    mov di,1
      
    isi_array_offset_cetak_data_tertentu:
      mov ah,isifile2[si]
      cmp isifile2[si],'|'
      jne exit5
      
      mov isifile2[si],'$'
      add si,1
      mov array_posisi[di],si
      sub si,1
      inc di
      inc jumlah_data
      
      exit5:
      inc si
      cmp cl,0 
      je exit_isi_array_cetak
    loop isi_array_offset_cetak_data_tertentu
    
    exit_isi_array_cetak:
    mov cl,jumlah_data
    mov si,0
        
    cek_data_dalam_array_cetak_data_tertentu:
      push si
      push cx
      mov dx, offset isifile2
      add dx, array_posisi[si]
      mov dh,05h
      mov di,dx
      mov si,offset inputan_nama[2]
      mov cl,inputan_nama[1]
      add dl,cl
      add dl,1
      
      rep cmpsb
      
      je ada_data_cetak:
      pop cx
      pop si
      inc si
      cmp cl,0
      je exit_cek_data_dalam_array_cetak_data_tertentu    
    loop cek_data_dalam_array_cetak_data_tertentu
    exit_cek_data_dalam_array_cetak_data_tertentu:

    mov ah, 9h
    mov dx, offset errormessage
    int 21h
    
    jmp menu_utama
    
    ada_data_cetak:
    mov ah,9h
    mov dx,offset enter2
    int 21h
    
    mov ah, 9h
    mov dx, offset isifile2
    pop si
    pop si
    add dx, array_posisi[si]
    mov dh,05h
    int 21h
    
    mov ah,9h
    mov dx,offset enter2
    int 21h
    
    jmp menu_utama   
ret

waow dw 10,13,"ada",10,13,"$"
enter2 db 10,13,"$" 
  
menu proc
    mov ah,9
    mov dl,offset tampilan_menu
    int 21h
ret

minta_input proc
    mov ah,1
    int 21h
    mov ch,al
    sub ch,30h
    mov ah,9
    mov dl,offset enter
    int 21h     
ret

reset proc
   mov ax,0
   mov bx,0
   mov cx,0
   mov dx,0 
ret

